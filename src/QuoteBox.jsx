import React, { Component } from "react";
import axios from "axios";
import FAIcon from "@fortawesome/react-fontawesome";
import faSpinner from "@fortawesome/fontawesome-free-solid/faSpinner";
import faTwitter from "@fortawesome/fontawesome-free-brands/faTwitter";


axios.defaults.baseURL = "https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous&count=1";
axios.defaults.headers["X-Mashape-Key"] = "rsEuw3s8x1mshBSPBRfBmlcavQ29p1Nxw5PjsnRdHbknb0Zpr4";
axios.defaults.headers["Accept"] = "application/json";

class QuoteBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quote: "",
      author: ""
    };

    this.getQuote = this.getQuote.bind(this);
    this.getQuote();
  }

  getQuote() {
    this.setState({
      quote: "",
      author: ""
    });

    axios.get()
      .then(({ data }) => {
        this.setState({
          quote: data[0].quote,
          author: data[0].author
        });
      });
  }

  twitterURL() {
    return `https://twitter.com/intent/tweet?text=${escape(`"${this.state.quote}"\n- ${this.state.author}`)}`;
  }

  render() {
    let quote =
      <div>
        <q id="text">{this.state.quote}</q>
        <cite id="author">{this.state.author}</cite>
      </div>;
    let loader =
      <div className="loader">
        <FAIcon icon={faSpinner} pulse />
        <p>Loading...</p>
      </div>

    return (
      <div id="quote-box">
        { this.state.quote.length ? quote : loader }
        <a className="button" href={this.twitterURL()} id="tweet-quote" target="_blank">
          <FAIcon icon={faTwitter} />
        </a>
        <button className="button" id="new-quote" onClick={this.getQuote}>New Quote</button>
      </div>
    );
  }
}

export default QuoteBox;
