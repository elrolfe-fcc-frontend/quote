import React, { Component } from "react";
import Credit from "./Credit";
import QuoteBox from "./QuoteBox";

class App extends Component {
  render() {
    return (
      <div className="App">
        <QuoteBox />
        <Credit />
        <footer>
          <p>Coded by <a href="https://www.freecodecamp.org/elrolfe" target="_blank" rel="noopener noreferrer">Eric Rolfe</a></p>
        </footer>
      </div>
    );
  }
}

export default App;
